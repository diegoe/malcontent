# French translations for malcontent package.
# Copyright (C) 2020 THE malcontent'S COPYRIGHT HOLDER
# This file is distributed under the same license as the malcontent package.
# Automatically generated, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: malcontent\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-15 13:45+0100\n"
"PO-Revision-Date: 2020-04-15 13:42+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: accounts-service/com.endlessm.ParentalControls.policy.in:4
msgid "Change your own app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:5
msgid "Authentication is required to change your app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:14
msgid "Read your own app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:15
msgid "Authentication is required to read your app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:24
msgid "Change another user’s app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:25
msgid "Authentication is required to change another user’s app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:34
msgid "Read another user’s app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:35
msgid "Authentication is required to read another user’s app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:44
msgid "Change your own session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:45
msgid "Authentication is required to change your session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:54
msgid "Read your own session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:55
msgid "Authentication is required to read your session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:64
msgid "Change another user’s session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:65
msgid "Authentication is required to change another user’s session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:74
msgid "Read another user’s session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:75
msgid "Authentication is required to read another user’s session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:84
msgid "Change your own account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:85
msgid "Authentication is required to change your account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:94
msgid "Read your own account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:95
msgid "Authentication is required to read your account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:104
msgid "Change another user’s account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:105
msgid "Authentication is required to change another user’s account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:114
msgid "Read another user’s account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:115
msgid "Authentication is required to read another user’s account info."
msgstr ""

#: libmalcontent/app-filter.c:694
#, c-format
msgid "App filter for user %u was in an unrecognized format"
msgstr ""

#: libmalcontent/app-filter.c:725
#, c-format
msgid "OARS filter for user %u has an unrecognized kind ‘%s’"
msgstr ""

#: libmalcontent/manager.c:283 libmalcontent/manager.c:412
#, c-format
msgid "Not allowed to query app filter data for user %u"
msgstr ""

#: libmalcontent/manager.c:288
#, c-format
msgid "User %u does not exist"
msgstr ""

#: libmalcontent/manager.c:394
msgid "App filtering is globally disabled"
msgstr ""

#: libmalcontent/manager.c:777
msgid "Session limits are globally disabled"
msgstr ""

#: libmalcontent/manager.c:795
#, c-format
msgid "Not allowed to query session limits data for user %u"
msgstr ""

#: libmalcontent/session-limits.c:306
#, c-format
msgid "Session limit for user %u was in an unrecognized format"
msgstr ""

#: libmalcontent/session-limits.c:328
#, c-format
msgid "Session limit for user %u has an unrecognized type ‘%u’"
msgstr ""

#: libmalcontent/session-limits.c:346
#, c-format
msgid "Session limit for user %u has invalid daily schedule %u–%u"
msgstr ""

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:76
msgid "No cartoon violence"
msgstr "Aucune violence de dessins animés"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:79
msgid "Cartoon characters in unsafe situations"
msgstr "Personnages de dessins animés en situation de danger"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:82
msgid "Cartoon characters in aggressive conflict"
msgstr "Personnages de dessins animés en conflit agressif"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:85
msgid "Graphic violence involving cartoon characters"
msgstr "Illustration de violences impliquant les personnages de dessins animés"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:88
msgid "No fantasy violence"
msgstr "Pas de violences fantastiques"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:91
msgid "Characters in unsafe situations easily distinguishable from reality"
msgstr "Personnages en situations dangereuses franchement irréelles"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:94
msgid "Characters in aggressive conflict easily distinguishable from reality"
msgstr "Personnages en situation de conflit agressif franchement irréel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:97
msgid "Graphic violence easily distinguishable from reality"
msgstr "Illustration violente franchement irréelle"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:100
msgid "No realistic violence"
msgstr "Aucune violence réaliste"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:103
msgid "Mildly realistic characters in unsafe situations"
msgstr "Personnages moyennement réalistes en situation dangereuse"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:106
msgid "Depictions of realistic characters in aggressive conflict"
msgstr "Illustrations de personnages réalistes en conflit agressif"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:109
msgid "Graphic violence involving realistic characters"
msgstr "Illustrations de violences impliquant des personnages réalistes"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:112
msgid "No bloodshed"
msgstr "Aucun massacre"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:115
msgid "Unrealistic bloodshed"
msgstr "Massacre irréaliste"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:118
msgid "Realistic bloodshed"
msgstr "Massacre réaliste"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:121
msgid "Depictions of bloodshed and the mutilation of body parts"
msgstr "Illustrations d’un massacre et de mutilations corporelles"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:124
msgid "No sexual violence"
msgstr "Aucune violence sexuelle"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:127
msgid "Rape or other violent sexual behavior"
msgstr "Viol ou autre comportement sexuel violent"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:130
msgid "No references to alcohol"
msgstr "Aucune allusion à des boissons alcoolisées"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:133
msgid "References to alcoholic beverages"
msgstr "Allusions à des boissons alcoolisées"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:136
msgid "Use of alcoholic beverages"
msgstr "Usage de boissons alcoolisées"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:139
msgid "No references to illicit drugs"
msgstr "Aucune allusion à des stupéfiants illicites"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:142
msgid "References to illicit drugs"
msgstr "Allusions à des stupéfiants illicites"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:145
msgid "Use of illicit drugs"
msgstr "Usage de stupéfiants illicites"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:148
msgid "References to tobacco products"
msgstr "Allusions à des produits dérivés du tabac"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:151
msgid "Use of tobacco products"
msgstr "Usage de produits dérivés du tabac"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:154
msgid "No nudity of any sort"
msgstr "Aucun nu d’aucune sorte"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:157
msgid "Brief artistic nudity"
msgstr "Nu artistique de courte durée"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:160
msgid "Prolonged nudity"
msgstr "État de nudité prolongée"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:163
msgid "No references or depictions of sexual nature"
msgstr "Aucune allusion ou image à caractère sexuel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:166
msgid "Provocative references or depictions"
msgstr "Allusions ou images provocatrices"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:169
msgid "Sexual references or depictions"
msgstr "Allusions ou images à caractère sexuel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:172
msgid "Graphic sexual behavior"
msgstr "Illustrations de comportements sexuels"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:175
msgid "No profanity of any kind"
msgstr "Aucune profanation d’aucune sorte"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:178
msgid "Mild or infrequent use of profanity"
msgstr "Utilisation modérée ou occasionnelle d’injures"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:181
msgid "Moderate use of profanity"
msgstr "Utilisation modérée d’injures"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:184
msgid "Strong or frequent use of profanity"
msgstr "Utilisation forte ou fréquente d’injures"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:187
msgid "No inappropriate humor"
msgstr "Aucun humour déplacé"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:190
msgid "Slapstick humor"
msgstr "Humour burlesque"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:193
msgid "Vulgar or bathroom humor"
msgstr "Humour vulgaire ou de caniveau"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:196
msgid "Mature or sexual humor"
msgstr "Humour pour adultes ou sexuel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:199
msgid "No discriminatory language of any kind"
msgstr "Aucune allusion discriminatoire d’aucune sorte"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:202
msgid "Negativity towards a specific group of people"
msgstr "Attitudes négatives vis à vis de groupes spécifiques de gens"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:205
msgid "Discrimination designed to cause emotional harm"
msgstr "Discriminations destinées à blesser émotionnellement"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:208
msgid "Explicit discrimination based on gender, sexuality, race or religion"
msgstr ""
"Discriminations explicites basées sur le genre, le sexe, la race ou la "
"religion"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:211
msgid "No advertising of any kind"
msgstr "Aucune publicité d’aucune sorte"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:214
msgid "Product placement"
msgstr "Placement de produits"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:217
msgid "Explicit references to specific brands or trademarked products"
msgstr "Allusions explicites à des produits de marque spécifique ou déposée"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:220
msgid "Users are encouraged to purchase specific real-world items"
msgstr ""
"Les utilisateurs sont encouragés à acheter des éléments spécifiques du monde "
"réel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:223
msgid "No gambling of any kind"
msgstr "Aucun pari d’aucune sorte"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:226
msgid "Gambling on random events using tokens or credits"
msgstr "Paris sur des événements aléatoires à l’aide de jetons ou à crédit"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:229
msgid "Gambling using “play” money"
msgstr "Paris avec de la monnaie « fictive »"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:232
msgid "Gambling using real money"
msgstr "Paris avec du vrai argent"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:235
msgid "No ability to spend money"
msgstr "Aucune possibilité de dépenser de l’argent"

#. v1.1
#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:238
msgid "Users are encouraged to donate real money"
msgstr "Les utilisateurs sont encouragés à donner de l’argent réel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:241
msgid "Ability to spend real money in-game"
msgstr "Possibilité de dépenser du vrai argent dans le jeu"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:244
msgid "No way to chat with other users"
msgstr "Aucune possibilité de discuter avec les autres utilisateurs"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:247
msgid "User-to-user game interactions without chat functionality"
msgstr "Actions de jeu entre utilisateurs sans possibilité de discussion"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:250
msgid "Moderated chat functionality between users"
msgstr "Possibilité modérée de discuter entre utilisateurs"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:253
msgid "Uncontrolled chat functionality between users"
msgstr "Possibilité de discuter sans contrôle entre utilisateurs"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:256
msgid "No way to talk with other users"
msgstr "Aucun moyen de parler avec les autres utilisateurs"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:259
msgid "Uncontrolled audio or video chat functionality between users"
msgstr "Possibilité de discuter ou se voir sans contrôle entre utilisateurs"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:262
msgid "No sharing of social network usernames or email addresses"
msgstr ""
"Aucun partage des noms d’utilisateur de réseaux sociaux ou des adresses "
"courriel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:265
msgid "Sharing social network usernames or email addresses"
msgstr ""
"Partage des noms d’utilisateur de réseaux sociaux ou des adresses courriel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:268
msgid "No sharing of user information with 3rd parties"
msgstr "Aucun partage des identifiants d’utilisateur avec des tierces parties"

#. v1.1
#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:271
msgid "Checking for the latest application version"
msgstr "Vérification s’il s’agit de la dernière version de l’application"

#. v1.1
#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:274
msgid "Sharing diagnostic data that does not let others identify the user"
msgstr ""
"Partage des données de diagnostic ne permettant pas l’identification de "
"l’utilisateur"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:277
msgid "Sharing information that lets others identify the user"
msgstr "Partage d’informations permettant l’identification de l’utilisateur"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:280
msgid "No sharing of physical location to other users"
msgstr "Aucun partage de géolocalisation avec les autres utilisateurs"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:283
msgid "Sharing physical location to other users"
msgstr "Partage de géolocalisation avec les autres utilisateurs"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:288
msgid "No references to homosexuality"
msgstr "Aucune allusion à l’homosexualité"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:291
msgid "Indirect references to homosexuality"
msgstr "Allusions indirectes à l’homosexualité"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:294
msgid "Kissing between people of the same gender"
msgstr "Étreintes entre personnes d’un même sexe"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:297
msgid "Graphic sexual behavior between people of the same gender"
msgstr "Images de comportements sexuels entre personnes d’un même sexe"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:300
msgid "No references to prostitution"
msgstr "Aucune allusion à la prostitution"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:303
msgid "Indirect references to prostitution"
msgstr "Allusions indirectes à la prostitution"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:306
msgid "Direct references to prostitution"
msgstr "Allusions directes à la prostitution"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:309
msgid "Graphic depictions of the act of prostitution"
msgstr "Images d’actes de prostitution"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:312
msgid "No references to adultery"
msgstr "Aucune allusion à l’adultère"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:315
msgid "Indirect references to adultery"
msgstr "Allusions indirectes à l’adultère"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:318
msgid "Direct references to adultery"
msgstr "Allusions directes à l’adultère"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:321
msgid "Graphic depictions of the act of adultery"
msgstr "Images d’actes d’adultère"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:324
msgid "No sexualized characters"
msgstr "Représentations humaines à caractère non sexuel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:327
msgid "Scantily clad human characters"
msgstr "Représentations humaines légèrement vêtues"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:330
msgid "Overtly sexualized human characters"
msgstr "Représentations humaines à caractère ouvertement sexuel"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:333
msgid "No references to desecration"
msgstr "Aucune allusion à de la profanation"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:336
msgid "Depictions or references to historical desecration"
msgstr "Allusions ou images de profanations historiques"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:339
msgid "Depictions of modern-day human desecration"
msgstr "Représentations d’actes de profanation contemporains sur humains"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:342
msgid "Graphic depictions of modern-day desecration"
msgstr "Images d’actes de profanation contemporains sur humains"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:345
msgid "No visible dead human remains"
msgstr "Aucune image de restes humains"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:348
msgid "Visible dead human remains"
msgstr "Images de restes humains"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:351
msgid "Dead human remains that are exposed to the elements"
msgstr "Restes humains exposés aux éléments"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:354
msgid "Graphic depictions of desecration of human bodies"
msgstr "Images de profanations sur des corps humains"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:357
msgid "No references to slavery"
msgstr "Aucune allusion à l’esclavagisme"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:360
msgid "Depictions or references to historical slavery"
msgstr "Représentations ou allusions à l’esclavagisme historique"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:363
msgid "Depictions of modern-day slavery"
msgstr "Représentations d’esclavagisme contemporain"

#. TRANSLATORS: content rating description
#: libmalcontent-ui/gs-content-rating.c:366
msgid "Graphic depictions of modern-day slavery"
msgstr "Images d’esclavagisme contemporain"

#. Translators: the placeholder is a user’s full name
#: libmalcontent-ui/restrict-applications-dialog.c:220
#, c-format
msgid "Restrict %s from using the following installed applications."
msgstr ""

#: libmalcontent-ui/restrict-applications-dialog.ui:6
#: libmalcontent-ui/restrict-applications-dialog.ui:12
msgid "Restrict Applications"
msgstr ""

#: libmalcontent-ui/restrict-applications-selector.ui:24
msgid "No applications found to restrict."
msgstr ""

#. Translators: this is the full name for an unknown user account.
#: libmalcontent-ui/user-controls.c:234 libmalcontent-ui/user-controls.c:245
msgid "unknown"
msgstr ""

#: libmalcontent-ui/user-controls.c:330 libmalcontent-ui/user-controls.c:403
#: libmalcontent-ui/user-controls.c:676
msgid "All Ages"
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:491
#, c-format
msgid ""
"Prevents %s from running web browsers. Limited web content may still be "
"available in other applications."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:496
#, c-format
msgid "Prevents specified applications from being used by %s."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:501
#, c-format
msgid "Prevents %s from installing applications."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:506
#, c-format
msgid "Applications installed by %s will not appear for other users."
msgstr ""

#: libmalcontent-ui/user-controls.ui:17
msgid "Application Usage Restrictions"
msgstr ""

#: libmalcontent-ui/user-controls.ui:67
msgid "Restrict _Web Browsers"
msgstr ""

#: libmalcontent-ui/user-controls.ui:151
msgid "_Restrict Applications"
msgstr ""

#: libmalcontent-ui/user-controls.ui:230
msgid "Software Installation Restrictions"
msgstr ""

#: libmalcontent-ui/user-controls.ui:280
msgid "Restrict Application _Installation"
msgstr ""

#: libmalcontent-ui/user-controls.ui:365
msgid "Restrict Application Installation for _Others"
msgstr ""

#: libmalcontent-ui/user-controls.ui:450
msgid "Application _Suitability"
msgstr ""

#: libmalcontent-ui/user-controls.ui:472
msgid ""
"Restricts browsing or installation of applications to applications suitable "
"for certain ages or above."
msgstr ""

#. Translators: This is the title of the main window
#. Translators: the name of the application as it appears in a software center
#: malcontent-control/application.c:105 malcontent-control/main.ui:12
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:9
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:3
msgid "Parental Controls"
msgstr ""

#: malcontent-control/application.c:270
msgid "Copyright © 2019, 2020 Endless Mobile, Inc."
msgstr ""

#: malcontent-control/application.c:272
msgid "translator-credits"
msgstr ""

#: malcontent-control/application.c:276
msgid "Malcontent Website"
msgstr ""

#: malcontent-control/application.c:294
msgid "The help contents could not be displayed"
msgstr ""

#: malcontent-control/application.c:331
msgid "Failed to load user data from the system"
msgstr ""

#: malcontent-control/application.c:333
msgid "Please make sure that the AccountsService is installed and enabled."
msgstr ""

#: malcontent-control/carousel.ui:48
msgid "Previous Page"
msgstr ""

#: malcontent-control/carousel.ui:74
msgid "Next Page"
msgstr ""

#: malcontent-control/main.ui:92
msgid "Permission Required"
msgstr ""

#: malcontent-control/main.ui:106
msgid ""
"Permission is required to view and change user parental controls settings."
msgstr ""

#: malcontent-control/main.ui:147
msgid "No Child Users Configured"
msgstr ""

#: malcontent-control/main.ui:161
msgid ""
"No child users are currently set up on the system. Create one before setting "
"up their parental controls."
msgstr ""

#: malcontent-control/main.ui:173
msgid "Create _Child User"
msgstr ""

#: malcontent-control/main.ui:201
msgid "Loading…"
msgstr ""

#: malcontent-control/main.ui:264
msgid "_Help"
msgstr "Aid_e"

#: malcontent-control/main.ui:268
msgid "_About Parental Controls"
msgstr ""

#. Translators: the brief summary of the application as it appears in a software center.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:12
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:4
msgid "Set parental controls and monitor usage by users"
msgstr ""

#. Translators: These are the application description paragraphs in the AppData file.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:16
msgid ""
"Manage users’ parental controls restrictions, controlling how long they can "
"use the computer for, what software they can install, and what installed "
"software they can run."
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:39
msgid "The GNOME Project"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:50
msgid "Minor improvements to parental controls application UI"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:51
msgid "Translations to Ukrainian and Polish"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:58
msgid "Improve parental controls application UI and add icon"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:59
msgid "Support for indicating which accounts are parent accounts"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:66
msgid "Initial release of basic parental controls application"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:67
msgid "Support for setting app installation and run restrictions on users"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:74
msgid "Maintenance release of underlying parental controls library"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localise the semicolons! The list MUST also end with a semicolon!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:13
msgid ""
"parental controls;screen time;app restrictions;web browser restrictions;oars;"
"usage;usage limit;kid;child;"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:9
msgid "Manage parental controls"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:10
msgid "Authentication is required to read and change user parental controls"
msgstr ""

#: malcontent-control/user-selector.c:426
msgid "Your account"
msgstr "Votre compte"

#. Always allow root, to avoid a situation where this PAM module prevents
#. * all users logging in with no way of recovery.
#: pam/pam_malcontent.c:142 pam/pam_malcontent.c:188
#, c-format
msgid "User ‘%s’ has no time limits enabled"
msgstr ""

#: pam/pam_malcontent.c:151 pam/pam_malcontent.c:172
#, c-format
msgid "Error getting session limits for user ‘%s’: %s"
msgstr ""

#: pam/pam_malcontent.c:182
#, c-format
msgid "User ‘%s’ has no time remaining"
msgstr ""

#: pam/pam_malcontent.c:200
#, c-format
msgid "Error setting time limit on login session: %s"
msgstr ""
